import express from "express";
import UserModel from "../model/UserModel";
import TokenModel from "../model/TokenModel";
import AuthMiddleware from "../middleware/AuthMiddleware";

const AuthRouter = express.Router();

AuthRouter.post("/email/login", (req, res) => {
  const {email, password} = req.body;

  if(!email || !password) {
    return res.status(400).json('email and password are required');
  }

  const user = UserModel.getUser();

  if(email !== user.email || password !== user.password) {
    return res.status(401).json('password is wrong');
  }

  const token = TokenModel.generateToken();

  return res.status(200).json({user, token})

});

AuthRouter.post("/me", AuthMiddleware, (req, res) => {

  return res.status(200).send("Authorized");
});

export default AuthRouter;