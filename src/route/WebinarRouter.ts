import express from "express";
import WebinarModal from "../model/WebinarModal";
import AuthMiddleware from "../middleware/AuthMiddleware";

const WebinarRouter = express.Router();

WebinarRouter.get("/", (req, res) => {
  try {
    const page = req.query.page === undefined ? 1: Number(req.query.page);
    const perPage = req.query.per_page === undefined ? 12: Number(req.query.per_page);
    if(
      !Number.isInteger(page) ||
      !Number.isInteger(perPage)  ||
      page < 1 ||
      perPage < 1
    ) {
      return res.status(400).json('query is invalid');
    }

    const webinarListResult = WebinarModal.getWebinarList(page, perPage);
    return res.status(200).json(webinarListResult);
  } catch (error) {
    res.status(500).json('server error');
  }
})

WebinarRouter.get("/favourited", AuthMiddleware, (req, res) => {
  try {
    const favouritedListResult = WebinarModal.getFavouritedWebinarList();
    return res.status(200).json(favouritedListResult);
  } catch (error) {
    res.status(500).json('server error');
  }
})

export default WebinarRouter;