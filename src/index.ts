import express from "express";
import WebinarRouter from "./route/WebinarRouter";
import AuthRouter from "./route/AuthRouter";
import FavouritesRouter from "./route/FavouritesRouter";
import swaggerUi  from "swagger-ui-express";
import swaggerDocument from "./doc/swagger.json";
import open from "open";

const app = express();
app.use(express.json()) ;
const port = 3000

app.use('/posts', WebinarRouter);
app.use('/auth', AuthRouter);
app.use('/favourites', FavouritesRouter);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('*', (req, res) => {
  res.redirect('/api-docs')
})
app.listen(port, () => {
  open(`http://0.0.0.0:${port}/api-docs`)
})