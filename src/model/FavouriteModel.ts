import { readFileSync, writeFileSync } from 'fs';
import path from "path";

class FavouriteModel {
  static DATA_FILE_PATH = path.resolve(__dirname, '../data/favourites.json');

  static getFavouriteIdList(): string[] {
    try {
      const rawData = readFileSync(FavouriteModel.DATA_FILE_PATH);
      const rawJson = JSON.parse(rawData.toString('utf8'));
      return rawJson.ids;
    } catch (error) {
      return [];
    }
  }

  static isIdExist(id: string): boolean {
    return FavouriteModel.getFavouriteIdList().includes(id);
  }

  static addId(id: string) {
    const ids = FavouriteModel.getFavouriteIdList();
    const idSet = new Set(ids);
    idSet.add(id);
    const data = {
      ids: Array.from(idSet)
    }
    writeFileSync(FavouriteModel.DATA_FILE_PATH, JSON.stringify(data))
  }
  
  static removeId(id: string) {
    const ids = FavouriteModel.getFavouriteIdList();
    const idSet = new Set(ids);
    idSet.delete(id);
    const data = {
      ids: Array.from(idSet)
    }
    writeFileSync(FavouriteModel.DATA_FILE_PATH, JSON.stringify(data))
  }
}

export default FavouriteModel